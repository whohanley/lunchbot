import argparse
import os
import sys
import prompts

if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('prompts_dir', type=str)
    arg_parser.add_argument('history_file', type=str)
    args = arg_parser.parse_args()

    all_prompts = prompts.read_all_prompts(args.prompts_dir, '')
    unused_prompts = prompts.remove_used_prompts(all_prompts, args.history_file)

    for key in list(unused_prompts.keys()):
        sys.stdout.write(key + os.linesep)

    sys.exit(0)
