import argparse
import json
import sys
import random

import prompts

def main(prompts_dir, history_file_path, channel):
    all_prompts = prompts.read_all_prompts(prompts_dir, channel)
    unused_prompts = prompts.remove_used_prompts(all_prompts, args.history_file)

    key, prompt = random.choice(list(unused_prompts.items()))

    out = prompt.as_rocketchat_jsonable()
    out['key'] = key

    return json.dumps(out)

if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('prompts_dir', type=str)
    arg_parser.add_argument('history_file', type=str)
    arg_parser.add_argument('rocketchat_channel', type=str)
    args = arg_parser.parse_args()

    sys.stdout.write(main(args.prompts_dir, args.history_file, args.rocketchat_channel))
    sys.exit(0)
