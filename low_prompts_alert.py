import argparse
import json
import os
import sys
import prompts

def write_rocketchat_json(channel, message):
    sys.stdout.write(json.dumps({
        'channel': channel,
        'text': message
    }))

if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('prompts_dir', type=str)
    arg_parser.add_argument('history_file', type=str)
    arg_parser.add_argument('rocketchat_channel', type=str)
    args = arg_parser.parse_args()

    all_prompts = prompts.read_all_prompts(args.prompts_dir, args.rocketchat_channel)
    unused_prompts = prompts.remove_used_prompts(all_prompts, args.history_file)

    msg_text = ''

    if len(unused_prompts) <= 0:
        write_rocketchat_json(args.rocketchat_channel, "I'm out of prompts. Talk to @whohanley if you want to contribute some.")
    elif len(unused_prompts) <= 5:
        write_rocketchat_json(args.rocketchat_channel, f"I'm low on prompts! I only have {len(unused_prompts)} left. Talk to @whohanley if you want to contribute some prompts.")

    sys.exit(0)
sys.stdout.write(json.dumps({
        'channel': args.rocketchat_channel,
        'body': {
            'text': msg_text
        }
    }))
