import mimetypes
import os

# Return a guess at a MIME type for the file at path. Special handling for
# .webp, which our mimetypes version doesn't support yet: https://bugs.python.org/issue38902
def guess_mime_type(path):
    if os.path.splitext(path)[1] == '.webp':
        return 'image/webp'

    mime_type, _ = mimetypes.guess_type(path)

    return mime_type

class Attachment:
    def __init__(self, path):
        self.path = path
        self.mime_type = guess_mime_type(path)

class TextMessage:
    def __init__(self, channel, text_file_path):
        self.channel = channel
        with open(text_file_path) as f:
            self.text = f.read().rstrip()

    def as_rocketchat_jsonable(self):
        return {
            'type': 'text',
            'body': {
                'channel': self.channel,
                'text': f"Today's lunchtime discussion prompt is: {self.text}"
            }
        }

class FileMessage:
    def __init__(self, path):
        self.attachment = Attachment(path)

    def as_rocketchat_jsonable(self):
        return {
            'type': 'file',
            'path': self.attachment.path,
            'mimetype': self.attachment.mime_type
        }

class DirectoryMessage:
    def __init__(self, dir_path):
        children = os.listdir(dir_path)
        if len([p for p in children if p != 'message.txt']) > 1:
            raise ValueError(f"{dir_path} contains more than one attachment.")

        for child_path in children:
            full_path = os.path.join(dir_path, child_path)
            if not os.path.isfile(full_path):
                raise IsADirectoryError(f"{dir_path} is not flat, contains subdirectory {child_path}")

            if child_path == 'message.txt':
                with open(full_path) as f:
                    self.message = f.read().rstrip();
            else:
                self.attachment = Attachment(full_path)

    def as_rocketchat_jsonable(self):
        return {
            'type': 'text_with_attachment',
            'path': self.attachment.path,
            'mimetype': self.attachment.mime_type,
            'msg': f"Today's lunchtime discussion prompt is: {self.message}"
        }

# Read prompts from prompts_dir. `channel` is required because the TextMessage
# constructor needs it because the Rocket.Chat API is just more convenient that
# way. If you aren't planning to actually post a prompt, you can pass an empty
# string.
def read_all_prompts(prompts_dir, channel):
    all_prompts = {}
    for child_path in os.listdir(prompts_dir):
        full_path = os.path.join(prompts_dir, child_path)
        if not os.path.isfile(full_path):
            all_prompts[child_path] = DirectoryMessage(full_path)
        elif os.path.splitext(full_path)[1] == '.txt':
            all_prompts[child_path] = TextMessage(channel, full_path)
        else:
            all_prompts[child_path] = FileMessage(full_path)

    return all_prompts

def remove_used_prompts(prompts, history_file_path):
    with open(history_file_path) as f:
        history_entries = {line.rstrip() for line in f.readlines()}

    unused_prompts = {key: prompt for key, prompt in prompts.items() if key not in history_entries}

    return unused_prompts
